package com.market.sys.service.order;

import com.market.sys.service.order.dto.OrderDTO;
import com.market.sys.service.order.dto.OrderItemDTO;

public interface OrderService {

    void saveOrderItem(OrderItemDTO orderItem, int i);

    OrderItemDTO selectByOrderId(int i);

   int insertSelective(OrderDTO orderDTO);
}
