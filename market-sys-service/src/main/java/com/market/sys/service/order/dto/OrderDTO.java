package com.market.sys.service.order.dto;

import lombok.Data;

import java.util.Date;

@Data
public class OrderDTO {
    private Integer orderId;

    private Integer userId;

    private Integer configId;

    private String remark;

    private Date lastModifyTime;
}
