package com.market.sys.service.order.impl;

import com.market.sys.dao.mapper.OrderItemMapper;
import com.market.sys.dao.mapper.OrderMapper;
import com.market.sys.dao.po.OrderItem;
import com.market.sys.dao.po.OrderPO;
import com.market.sys.service.order.OrderService;
import com.market.sys.service.order.dto.OrderDTO;
import com.market.sys.service.order.dto.OrderItemDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderItemMapper orderItemMapper;

    @Autowired
    OrderMapper orderMapper;

    @Override
    public void saveOrderItem(OrderItemDTO orderItemDTO, int i) {
        OrderItem orderItem = new OrderItem();
        BeanUtils.copyProperties(orderItemDTO,OrderItem.class);
        orderItemMapper.save(orderItem);
    }

    @Override
    public OrderItemDTO selectByOrderId(int i) {

        return null;
    }

    @Override
    public int insertSelective(OrderDTO orderDTO) {
        OrderPO orderPO = new OrderPO();
        BeanUtils.copyProperties(orderDTO,orderPO);
        return orderMapper.insertSelective(orderPO);
    }
}
