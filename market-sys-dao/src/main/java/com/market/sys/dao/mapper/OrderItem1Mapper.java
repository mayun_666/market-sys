package com.market.sys.dao.mapper;

import com.market.sys.dao.po.OrderItem1;
import tk.mybatis.mapper.common.BaseMapper;

public interface OrderItem1Mapper extends BaseMapper<OrderItem1> {
}