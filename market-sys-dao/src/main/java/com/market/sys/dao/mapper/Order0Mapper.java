package com.market.sys.dao.mapper;

import com.market.sys.dao.po.Order0;
import tk.mybatis.mapper.common.BaseMapper;

public interface Order0Mapper extends BaseMapper<Order0> {
}