package com.market.sys.dao.mapper;

import com.market.sys.dao.po.OrderItem;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface OrderItemMapper {
    @Insert("insert into t_order_item(order_id,remark) values(#{orderId},#{remark})")
    Integer save(OrderItem orderItem);

    @Select("select order_id orderId  from t_order_item " +
            "where  order_id = #{orderId}")
    OrderItem selectByOrderId(Integer orderId);
}