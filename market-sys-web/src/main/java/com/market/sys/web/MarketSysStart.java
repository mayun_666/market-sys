package com.market.sys.web;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan(basePackages = {"com.market","com.market.sys.dao"})
@MapperScan("com.market.sys.dao")
public class MarketSysStart {
    public static void main(String[] args) {
        SpringApplication.run(MarketSysStart.class, args);
    }
}
