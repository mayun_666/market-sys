package com.market.sys.web.order;

import com.market.sys.service.order.OrderService;
import com.market.sys.service.order.dto.OrderDTO;
import com.market.sys.service.order.dto.OrderItemDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class ItemController {

    @Autowired
    private OrderService orderService;

    @RequestMapping("/test")
    public String test(Integer orderId){

        OrderItemDTO orderItem = new OrderItemDTO();
        orderItem.setOrderId(orderId);
        orderItem.setRemark("tes1t");
        orderService.saveOrderItem(orderItem,1);

        return "ss";
    }

    @RequestMapping("/get")
    public OrderItemDTO getOrderItem(){
        return  orderService.selectByOrderId(1);
    }

    @RequestMapping("/add")
    public int add(Integer orderId){
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setUserId(orderId);
        orderDTO.setOrderId(orderId);
        orderDTO.setConfigId(orderId);
        orderDTO.setLastModifyTime(new Date());
        orderDTO.setRemark("setRemark"+orderId);
        return  orderService.insertSelective(orderDTO);
    }
}
